#include "tools/CLASS_NAME_TEMPLATE.h"

using namespace Ph2_HwDescription;
using namespace Ph2_HwInterface;
using namespace Ph2_System;

CLASS_NAME_TEMPLATE::CLASS_NAME_TEMPLATE() : Tool() {}

CLASS_NAME_TEMPLATE::~CLASS_NAME_TEMPLATE() {}

void CLASS_NAME_TEMPLATE::Initialise(void)
{
   

#ifdef __USE_ROOT__ // to disable and anable ROOT by command
    // Calibration is not running on the SoC: plots are booked during initialization
    fDQMHistogramCLASS_NAME_TEMPLATE.book(fResultFile, *fDetectorContainer, fSettingsMap);
#endif
}

void CLASS_NAME_TEMPLATE::ConfigureCalibration()
{

}

void CLASS_NAME_TEMPLATE::Running()
{
    LOG(INFO) << "Starting CLASS_NAME_TEMPLATE measurement.";
    Initialise();
    LOG(INFO) << "Done with CLASS_NAME_TEMPLATE.";
}

void CLASS_NAME_TEMPLATE::Stop(void)
{
    LOG(INFO) << "Stopping CLASS_NAME_TEMPLATE measurement.";
    #ifdef __USE_ROOT__
        // Calibration is not running on the SoC: processing the histograms
        fDQMHistogramCLASS_NAME_TEMPLATE.process();
    #endif
    dumpConfigFiles();
    SaveResults();
    closeFileHandler();
    LOG(INFO) << "CLASS_NAME_TEMPLATE stopped.";
}

void CLASS_NAME_TEMPLATE::Pause()
{

}


void CLASS_NAME_TEMPLATE::Resume()
{

}
